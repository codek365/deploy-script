#!/bin/bash
DB_PASS=$(cat /etc/cyberpanel/mysqlPassword)

git_repo=git@bitbucket.org:afterz/sodaz.git
home=/home/source-sync

repo="${home}"/repo/

backup="${home}"/backup/"$(date)"

branch=release/1.0.3

ver=1.0.3

dest=release/"${ver}"

array=( tt mh )

domain=sod.asia

check_git () {
    if [ -d "${repo}" ]; then
        cd repo
        git fetch
        git checkout "${branch}"
        git pull

        [ -d "${backup}" ]  || mkdir "${backup}"
        rsync   -avz -u --stats --ignore-existing --progress --delete "${repo}" "${backup}" \
                --exclude '.git'
    else
        echo "repo not exits"
        git clone "${git_repo}" repo
    fi    
}

start_sync () {
    if [ -d "${repo}" ]; then
        rsync -avz -u --stats --ignore-existing --progress --delete "${repo}" "$1" \
        --exclude '.git'
    fi
}

sync () {
    check_git
    for i in "${array[@]}"
    do
        target=/home/$i."${domain}"/public_html/"${dest}"
        [ -d "${target}" ] || mkdir "${target}"
        start_sync "${target}"
        chmod 755 "${target}"/install.sh
        chown -R "${i}"sodas:"${i}"sodas "${target}"
        if [ -f "${target}/.env" ]; then
            echo file exists
        else
            echo file not exists
            cd $target
            # su "${i}"sodas -c "./install.sh pd"
            su "${i}"sodas -c "./install.sh s2"
        fi
        
    done
}

deploy () {
    check_git

    for i in "${array[@]}"
    do
        target=/home/$i."${domain}"/public_html/"${dest}"
        [ -d "${target}" ] || mkdir "${target}"
        start_sync "${target}"
        chmod 755 "${target}"/install.sh
        chown -R "${i}"sodas:"${i}"sodas "${target}"
        su "${i}"sodas -c "composer install"
    done
}

echo $DB_PASS

case $1 in
"sync")
sync
;;
"deploy")
deploy
;;
"start_sync")
start_sync
;;
"check_git")
check_git
;;
esac